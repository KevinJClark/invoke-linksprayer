# Invoke-LinkSpray

Powershell script to create malicious SMB or WebDAV links to steal NTLM authentication. Takes UNC or local folder paths from a file specified by the `-ShareList` parameter. Optionally, `-Depth` can be used to specify how many folders to recurse when creating URL files. Also supports a `-RestoreFile` parameter to clean up all files after you're done.

A typical run of the program -- Creating URL files
![PSLinkSpray](/uploads/7f1d86297149fabff14510f4a916166e/PSLinkSpray.png)

Hashes collected from the previous command in Responder
![LinkSprayHashes](/uploads/da5fa1f62ad135166ef420e68c62b3b3/LinkSprayHashes.png)

Restoring and cleaning up all links
![PSLinkRestore](/uploads/a95d3f5228a28d40f318c44ce31c1c96/PSLinkRestore.png)